<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Auth\LoginController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('dashboard');
Route::get('/user-profile', 'HomeController@user_profile')->name('profile');
Route::get('/create-new-application', 'HomeController@create_new_application')->name('createapp');

// Authentication Routes...
Route::get('/login', function(){
    return redirect()->route('login');
});
Route::get('/login-page', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('/register', function(){
    return redirect()->route('register');
});
Route::get('/register-account', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register');
//end
// Password Reset Routes...
Route::get('/forget-password', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password-email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password-reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password-reset', 'Auth\ResetPasswordController@reset');
//end
//Phone veryify
Route::get('/phone-verification', 'Auth\PhoneNumberController@phone_verify_view')->name('password.phone_verify');
Route::post('/phone-verification', 'Auth\PhoneNumberController@phone_verify');
Route::get('/edit-phone', 'Auth\PhoneNumberController@phone_edit_view')->name('password.phone_edit');
Route::post('/edit-phone', 'Auth\PhoneNumberController@phone_edit');

//site pages
Route::get('terms-and-condition', 'PageController@termsCondition')->name('terms');
Route::get('about-us', 'PageController@about_us')->name('about_us');
Route::get('privacy-policy', 'PageController@privacy_policy')->name('privacy');
Route::get('how-to-promote-mobile-apps', 'PageController@how_it_work')->name('how_work');

//change user info
Route::post('/change-name', 'HomeController@change_name')->name('change_name');
Route::post('/change-avatar', 'HomeController@change_avatar')->name('change_avatar');
Route::post('/change-password', 'HomeController@change_password')->name('change_password');
Route::post('/change-phone', 'HomeController@change_phone')->name('change_phone');
Route::post('/change-email', 'HomeController@change_email')->name('change_email');
Route::post('/change-company', 'HomeController@change_company')->name('change_company');

Route::prefix('admin')->group(function(){
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/login', 'Admin\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Admin\AdminLoginController@login');
    Route::get('/logout', 'Admin\AdminLoginController@logout')->name('admin.logout');
    //Admin Password Reset Routes...
    Route::get('/forget-password', 'Admin\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/email', 'Admin\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password-reset/{token}', 'Admin\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('/password-reset', 'Admin\AdminResetPasswordController@reset');
    //end
    Route::get('/create-new-application', 'AdminController@create_new_application')->name('admin.createapp');
    Route::get('/admin-profile', 'AdminController@admin_profile')->name('admin.profile');
    Route::get('/pages', 'AdminController@admin_pages')->name('admin.pages');
    Route::get('/user-management', 'AdminController@user_management')->name('admin.user_management');
    Route::post('/delete-user', 'AdminController@delete_user')->name('admin.user_delete');
    Route::get('/admin-management', 'AdminController@admin_management')->name('admin.admin_management');
    Route::post('/add-admin', 'AdminController@add_admin')->name('admin.add_admin');
    Route::post('/get-admin-info', 'AdminController@get_admin_info')->name('admin.get_admin_info');
    Route::post('/edit-admin', 'AdminController@edit_admin')->name('admin.edit_admin');
    Route::post('/delete-admin', 'AdminController@delete_admin')->name('admin.admin_delete');
    //change admin info
    Route::post('/change-admin-name', 'AdminController@change_admin_name')->name('admin.change_admin_name');
    Route::post('/change-admin-avatar', 'AdminController@change_avatar')->name('admin.change_avatar');
    Route::post('/change-password', 'AdminController@change_password')->name('admin.change_password');
    Route::post('/change-phone', 'AdminController@change_phone')->name('admin.change_phone');
    Route::post('/change-email', 'AdminController@change_email')->name('admin.change_email');
    Route::post('/change-company', 'AdminController@change_company')->name('admin.change_company');
    Route::post('/email-active', 'AdminController@email_active')->name('admin.email_activate');
    Route::post('/phone-active', 'AdminController@phone_active')->name('admin.phone_activate');
    //end
    Route::get('/get-page-info', 'PageController@get_page_info')->name('admin.get_page_info');
    Route::post('/edit-term', 'PageController@update_terms')->name('admin.update_terms');
    Route::post('/edit-privacy', 'PageController@update_privacy')->name('admin.update_privacy');
    Route::post('/edit-aboutus', 'PageController@update_aboutus')->name('admin.update_aboutus');
    Route::post('/edit-how-work', 'PageController@update_howitwork')->name('admin.update_howitwork');
});

Route::get('email-verification/error', 'Auth\RegisterController@getVerificationError')->name('email-verification.error');
Route::get('email-verification/check/{token}', 'Auth\RegisterController@getVerification')->name('email-verification.check');
