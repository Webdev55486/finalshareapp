@extends('admin.adminMaster')
@section('title')
User Management
@endsection
@section('pagelevel_plugin')
<link href="{{ cdn('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ cdn('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ cdn('assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ cdn('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('content')
  <!-- BEGIN PAGE TITLE-->

  <!-- END PAGE TITLE-->
  <!-- END PAGE HEADER-->
  <div class="m-heading-1 border-green m-bordered">
      <h3>Admin Management page</h3>
      <p> In this page ...<br />1 . You can <a data-toggle="modal" href="#responsive" >add</a> new admin , edit admin and delete.<br />
      <p><a class="btn btn-default green" data-toggle="modal" href="#responsive" >Add new Admin</a></p>
  </div>
  @if (session('status'))
      <div class="alert alert-success">
        <button class="close" data-close="alert"></button>
        <span>{{ session('status') }}</span>
      </div>
  @endif

  <div class="row">
      <div class="col-md-12">
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="portlet light bordered">
              <div class="portlet-title">
                  <div class="caption font-dark">
                      <i class="icon-settings font-dark"></i>
                      <span class="caption-subject bold uppercase"> Super Admin management</span>
                  </div>
              </div>
              <div class="portlet-body">
                  <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_2">
                      <thead>
                          <tr>
                              <th>
                                  <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                              </th>
                              <th> User Name </th>
                              <th> Email </th>
                              <th> Company Name </th>
                              <th> Phone Number </th>
                              <th> Level </th>
                              <th> Action </th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($admins as $admin)
                          @if($admin->user_role == 2)
                            <tr class="odd gradeX" id="userlist_{{$admin->id}}">
                                <td>
                                    <input type="checkbox" class="checkboxes" value="1" />
                                </td>
                                <td> {{$admin->name}}</td>
                                <td>
                                    <a href="mailto:{{$admin->email}}"> {{$admin->email}} </a>
                                </td>
                                <td> {{$admin->company_name}} </td>
                                <td class="center"> {{$admin->phone_number}} </td>
                                <td class="center">Super Admin</td>
                                <td align='center'>
                                  @if($admin->email == Auth::guard('admin')->user()->email)
                                    <a type="button" class="btn btn-default green user-managebutton-size" disabled="true">Edit</a>
                                    <a type="button" class="btn btn-default red-flamingo user-managebutton-size"  disabled="true">Delete</a>
                                  @elseif($admin->email != Auth::guard('admin')->user()->email)
                                    <a type="button" data-toggle="modal" href="#responsive1" onclick="edit_admin({{$admin->id}})" class="btn btn-default green user-managebutton-size">Edit</a>
                                    <a type="button" onclick="delete_admin({{$admin->id}})" class="btn btn-default red-flamingo user-managebutton-size">Delete</a>
                                  @endif
                                </td>
                            </tr>
                          @endif
                        @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
          <!-- END EXAMPLE TABLE PORTLET-->
      </div>
  </div>

  <div class="row">
      <div class="col-md-12">
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="portlet light bordered">
              <div class="portlet-title">
                  <div class="caption font-dark">
                      <i class="icon-settings font-dark"></i>
                      <span class="caption-subject bold uppercase"> Admin management</span>
                  </div>
              </div>
              <div class="portlet-body">
                  <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                      <thead>
                          <tr>
                              <th>
                                  <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                              </th>
                              <th> User Name </th>
                              <th> Email </th>
                              <th> Company Name </th>
                              <th> Phone Number </th>
                              <th> Level </th>
                              <th> Action </th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($admins as $admin)
                          @if($admin->user_role == 1)
                            <tr class="odd gradeX" id="userlist_{{$admin->id}}">
                                <td>
                                    <input type="checkbox" class="checkboxes" value="1" />
                                </td>
                                <td> {{$admin->name}}</td>
                                <td>
                                    <a href="mailto:{{$admin->email}}"> {{$admin->email}} </a>
                                </td>
                                <td> {{$admin->company_name}} </td>
                                <td class="center"> {{$admin->phone_number}} </td>
                                <td class="center">Admin</td>
                                <td align='center'>
                                    <a type="button" data-toggle="modal" href="#responsive1" onclick="edit_admin({{$admin->id}})" class="btn btn-default green user-managebutton-size">Edit</a>
                                    <a type="button" onclick="delete_admin({{$admin->id}})" class="btn btn-default red-flamingo user-managebutton-size">Delete</a>
                                </td>
                            </tr>
                          @endif
                        @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
          <!-- END EXAMPLE TABLE PORTLET-->
      </div>
  </div>
  <!-- responsive -->
  <div id="responsive" class="modal fade" tabindex="-1" data-width="560">
      <form action="{{ route('admin.add_admin') }}" class="register-form" method="post">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h2 class="modal-title text-center">New Administrator</h2>
              <br />
              <h4 class="modal-title text-center">Enter detail below to create new admin user</h4>
          </div>
          <div class="modal-body">
              <div class="row">
                  <div class="col-md-12">
                      {{ csrf_field() }}
                      <div class="row">
                          <div class="col-sm-12">
                              <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Name" value="{{ old('name') }}" name="name" required/>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-6">
                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="email" autocomplete="off" placeholder="Email" value="{{ old('email') }}" name="email" required/>
                          </div>
                          <div class="col-sm-6">
                            <select class="form-control form-control-solid placeholder-no-fix form-group" name="userrole" style="width:100%;border:1px solid #bdbdbd;">
                                <option selected value="1">Admin</option>
                                <option value="2">Super Admin</option>
                            </select>
                          </div>
                      </div>
                      <div class="row">
                            <div class="col-sm-6">
                                <input class="form-control form-control-solid placeholder-no-fix form-group" id="register_password" type="password" autocomplete="off" placeholder="Password" name="password" required/>
                            </div>
                            <div class="col-sm-6">
                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Confirm Password" name="password_confirmation" required/>
                            </div>
                        </div>
                  </div>
              </div>
          </div>
          <div class="modal-footer text-center" style="text-align:center;">
              <button type="submit" class="btn green">Proceed</button>
          </div>
      </form>
  </div>

  <div id="responsive1" class="modal fade" tabindex="-1" data-width="560">
      <form action="{{ route('admin.edit_admin') }}" class="edit-form" method="post">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h2 class="modal-title text-center">Edit Administrator</h2>
              <br />
              <h4 class="modal-title text-center">Enter detail below to Edit admin user</h4>
          </div>
          <div class="modal-body">
              <div class="row">
                  <div class="col-md-12">
                      {{ csrf_field() }}
                      <input type="hidden" id="userid" name="userid" value="" />
                      <div class="row">
                          <div class="col-sm-12">
                              <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Name" value="{{ old('name') }}" name="name" id="name" required/>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-6">
                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="email" autocomplete="off" placeholder="Email" value="{{ old('email') }}" name="email" id="email" required/>
                          </div>
                          <div class="col-sm-6">
                            <select class="form-control form-control-solid placeholder-no-fix form-group" name="userrole" id="userrole" style="width:100%;border:1px solid #bdbdbd;">
                                <option  value="1">Admin</option>
                                <option selected value="2">Super Admin</option>
                            </select>
                          </div>
                      </div>
                      <div class="row">
                            <div class="col-sm-6">
                                <input class="form-control form-control-solid placeholder-no-fix form-group" id="edit_password" type="password" autocomplete="off" placeholder="Password" name="password"/>
                            </div>
                            <div class="col-sm-6">
                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Confirm Password" name="password_confirmation"/>
                            </div>
                        </div>
                  </div>
              </div>
          </div>
          <div class="modal-footer text-center" style="text-align:center;">
              <button type="submit" class="btn green">Proceed</button>
          </div>
      </form>
  </div>
@endsection
@section('pagelevel_script')
<script src="{{ cdn('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ cdn('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ cdn('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ cdn('assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
<script src="{{ cdn('assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
<script src="{{ cdn('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ cdn('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
@endsection
@section('pagelevel_script_script')
<script src="{{ cdn('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
<script src="{{ cdn('assets/pages/scripts/ui-extended-modals.min.js') }}" type="text/javascript"></script>
<script src="{{ cdn('assets/pages/scripts/login-5.js') }}" type="text/javascript"></script>
<script>
  function delete_admin(id){
    url = "{{ route('admin.admin_delete') }}";
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete user!",
      cancelButtonText: "No, cancel plx!",
      closeOnConfirm: false,
      closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
          axios.post(url, {admin_id:id})
          .then(function (response) {
              $("#userlist_"+ id).remove();
              swal("Deleted!", "Your imaginary file has been deleted.", "success");
          })
          .catch(function (error) {
            console.log(error);
          });
        } else {
            swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
    });
  };

  function edit_admin(id){
    url = "{{ route('admin.get_admin_info') }}";
    axios.post(url, {admin_id:id})
    .then(function (response) {
        console.log(response.data["id"]);
        $("#userid").val(response.data["id"]);
        $("#name").val(response.data["name"]);
        $("#email").val(response.data["email"]);
        $("#userrole").val(response.data["user_role"]);
    })
    .catch(function (error) {
      console.log(error);
    });
  }
</script>
@endsection
