@extends('auth.pageMaster')
@section('title')
About Us
@endsection
@section('helptitle')
About Us
@endsection
@section('content')
  <?php echo html_entity_decode($pagedata->about_us); ?>
@endsection
