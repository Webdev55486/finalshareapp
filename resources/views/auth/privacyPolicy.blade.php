@extends('auth.pageMaster')
@section('title')
Privacy policy
@endsection
@section('helptitle')
Privacy Policy
@endsection
@section('content')
  <?php echo html_entity_decode($pagedata->privacy_policy); ?>
@endsection
