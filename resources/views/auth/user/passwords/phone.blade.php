@extends('auth.user.userAuthmaster')
@section('title')
Phone Verify
@endsection
@section('content')
  <h1>Phone Number Verification</h1>
  <form class="forget-form" action="{{ url('phone-verification') }}" method="post">
      {{ csrf_field() }}
      <?php if(isset($user)) { ?>
      <input type="hidden" value="{{$user->id}}" name="userid" />
      <?php } ?>
      <p> Enter the verification code received at <?php if(isset($user)) { ?> {{$user->phone_number}} <?php } ?>. </p>
      @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
      @endif
      @if (session('error'))
          <div class="alert alert-danger">
              {{ session('error') }}
          </div>
      @endif
      <div class="form-group">
          <input class="form-control placeholder-no-fix form-group" type="text" autocomplete="off" value="{{ old('email') }}" placeholder="Verification code" name="phoneverifycode" /> </div>
      <div class="form-actions">
          <a class="btn grey btn-default" href="{{ route('password.phone_edit') }}">Edit Phone Number</a>
          <button type="submit" class="btn blue btn-success uppercase pull-right" <?php if(!isset($user)) {?> disabled="true" <?php } ?>>Submit</button>
      </div>
  </form>
  <!-- END FORGOT PASSWORD FORM -->
@endsection
