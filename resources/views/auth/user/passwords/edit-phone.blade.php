@extends('auth.user.userAuthmaster')
@section('title')
Edit Phone Number
@endsection
@section('content')
  <h1>Phone Number Verification</h1>
  <form action="{{ url('edit-phone') }}" class="register-form" method="post">
      {{ csrf_field() }}
      <?php if(isset($user)) { ?>
      <input type="hidden" value="{{$user->id}}" name="userid" />
      <?php } ?>
      <p style="color:#4c4b4b;"> Please enter your phone number below. A verification code will be sent ot this number.</p>
      @if (session('status'))
          <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            <span>{{ session('status') }}</span>
          </div>
      @endif
      <div class="row">
          <div class="col-sm-6">
            <select class="form-control form-control-solid placeholder-no-fix form-group" name="phoneCountry" style="width:100%;border:0;border-bottom:1px solid #a0a9b4;">
              @foreach($countries as $key => $country)
                <option <?php if($key == 'US') echo 'selected' ?> value="{{$key}}">{{$country}}</option>
              @endforeach
            </select>
          </div>
          <div class="col-sm-6">
            @if (session('status'))
                <input class="form-control form-control-solid placeholder-no-fix form-group has-error" type="number" autocomplete="off" placeholder="Phone Number" value="{{ old('phonenumber') }}" name="phonenumber" required/>
            @else
                <input class="form-control form-control-solid placeholder-no-fix form-group" type="number" autocomplete="off" placeholder="Phone Number" value="{{ old('phonenumber') }}" name="phonenumber" required/>
            @endif
          </div>
      </div>
      <div class="row">
          <div class="col-sm-4 text-left">
              <button class="btn blue" type="submit" <?php if(!isset($user)) {?> disabled="true" <?php } ?>>VERIFY</button>
          </div>
      </div>
  </form>
@endsection
