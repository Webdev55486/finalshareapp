<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191);
			$table->string('company_name', 191);
			$table->string('email', 191)->unique('users_email_unique');
			$table->string('phone_number', 50);
			$table->string('password', 191);
			$table->string('remember_token', 100)->nullable();
			$table->boolean('verified')->default(0);
			$table->string('verification_token', 191)->nullable();
			$table->integer('user_role')->nullable()->default(0);
			$table->timestamps();
			$table->boolean('phone_verified')->default(0);
			$table->integer('phone_code')->nullable();
			$table->string('country_code', 50)->nullable();
			$table->string('country_name', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admins');
	}

}
