<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSiteServiceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('site_service', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->text('terms_service')->nullable();
			$table->text('privacy_policy')->nullable();
			$table->text('about_us')->nullable();
			$table->text('how_it_work')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('site_service');
	}

}
