<?php

namespace App\Http\Controllers;

use Auth;
use \App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use PragmaRX\Countries\Facade as Countries;
use Propaganistas\LaravelIntl\Facades\Country;
use Propaganistas\LaravelPhone\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberType;
use SimpleSoftwareIO\SMS\Facades\SMS;
use GuzzleHttp\Client as Guzzle;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;
use Jrean\UserVerification\Facades\UserVerification as UserVerificationFacade;
use App\Mail\OrderShipped;
use Mail;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.admin-home');
    }

    public function create_new_application()
    {
      return view('admin.CreateNewApplication');
    }

    public function admin_profile()
    {
        $countrylists = [];
        $real_phone_number="";
        $current_phone_number = Auth::guard('admin')->user()->phone_number;
        $countries = Country::all();
        if(Auth::guard('admin')->user()->country_code && Auth::guard('admin')->user()->country_code != "unknown")
        {
            $current_dialNumber = Countries::where('cca2', Auth::guard('admin')->user()->country_code)->first()->callingCode[0];
            $dial_length = strlen($current_dialNumber);
            $real_phone_number = substr($current_phone_number,$dial_length);
        }else{
            $real_phone_number = $current_phone_number;
        }

        foreach ($countries as $key => $country) {
            try {
                $dialNumber = Countries::where('cca2', $key)->first()->callingCode[0];
                $countrylists[$key] = $country.' +'.$dialNumber;
            }
            catch (\Exception $ex) {
                continue;
            }
        }
      return view('admin.admin-profile',['countries' => $countrylists,'phone_number' => $real_phone_number]);
    }

    public function change_admin_name(Request $request)
    {
        if(Auth::guard('admin')->user()->id){
            $obj_admin = Auth::guard('admin')->user();
            if($request->input('name') != $obj_admin->name)
            {
                $obj_admin->name = $request->input('name');
                $obj_admin->save();
                return back()->with('status', 'User Name changed successfully');
            }
            return back()->with('error', 'You have already this User Name.');
        }
        return back()->with('error', 'Went somthing wrong');
    }

    public function change_company(Request $request)
    {
        if(Auth::guard('admin')->user()->id){
            $obj_admin = Auth::guard('admin')->user();
            if($obj_admin->company_name != $request->input('company'))
            {
                $obj_admin->company_name = $request->input('company');
                $obj_admin->save();
                return back()->with('status', 'Company Name changed successfully');
            }
            return back()->with('error', 'You have already this Company Name.');
        }
        return back()->with('error', 'Went somthing wrong');
    }

    public function change_avatar(Request $request)
    {
        if (Auth::guard('admin')->user()->id) {
            $this->validate($request, [
                'user_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            $imageRand = rand(1000, 9999);
            $admin = Auth::guard('admin')->user();
            $admin->image = Auth::guard('admin')->user()->id."_".$imageRand;
            $admin->save();

            $img = $request->user_image;
            $dst = public_path('assets/images/avatar/admin/') . Auth::guard('admin')->user()->image;
            $thumbnail = public_path('assets/images/avatar/admin/') . Auth::guard('admin')->user()->image.'_thumbnail';
            if (($img_info = getimagesize($img)) === FALSE)
                return back()->with('error', 'Image not found or not an image');

            $width = $img_info[0];
            $height = $img_info[1];

            switch ($img_info[2]) {
              case IMAGETYPE_GIF  : $src = imagecreatefromgif($img);  break;
              case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($img); break;
              case IMAGETYPE_PNG  : $src = imagecreatefrompng($img);  break;
              default : return back()->with('error', 'Unknown file type');
            }

            $tmp = imagecreatetruecolor($width, $height);
            $tmp_thumbnail = imagecreatetruecolor(200,  200);
            imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
            imagecopyresized($tmp_thumbnail, $src, 0, 0, 0, 0, 200, 200, $width, $height);
            imagejpeg($tmp, $dst . ".jpg");
            imagejpeg($tmp_thumbnail, $thumbnail . ".jpg");
            // $imageName = time().'.'.$request->image->getClientOriginalExtension();
            // $request->image->move(public_path('images/avatar'), $imageName);

            return back()->with('status', 'Your avatar changed successfully');
        } else {
            return back()->with('error', 'You have no access for this action');
        }
    }

    public function change_password(Request $request)
    {
        if (Auth::guard('admin')->check()) {
            $requestData = $request->All();
            $this->validateChangePassword($requestData);
            $current_password = Auth::guard('admin')->user()->password;
            if (Hash::Check($requestData['current-password'], $current_password)) {
                $obj_admin = Auth::guard('admin')->user();
                $obj_admin->password = bcrypt($requestData['password']);
                $obj_admin->save();
                return back()->with('status', 'Password changed successfully');
            } else {
                return back()->with('error', 'Please enter correct current password');
            }
        } else {
            return redirect()->to('/');
        }
    }

    private function validateChangePassword(array $data)
    {
        $messages = [
            'current-password.required' => 'Please enter current password',
            'password.required' => 'Please enter password',
            'password.regex' => 'Must contain at least one number and one uppercase and lowercase letter, and at least 6 or more characters'
        ];

        $validator = Validator::make($data, [
            'current-password' => 'required',
        ], $messages);

        return $validator->validate();
    }

    public function phoneValidation($phonenumber, $countrycode) {
        $fullNumber = '';
        $phonenumber = ltrim($phonenumber, '0');
        $dialNumber = Countries::where('cca2', $countrycode)->first()->callingCode[0];

        if (strlen($phonenumber) < 9 || strlen($phonenumber) > 11) {
            return false;
        }
        else if (strlen($phonenumber) <= 11 && strlen($phonenumber) >= 9 ) {
            if (strpos($phonenumber, $dialNumber) === 0)
            return false;

            $fullNumber = $dialNumber.$phonenumber;
        }
        else {
            if (strpos($phonenumber, $dialNumber) !== 0)
            return false;

            $fullNumber = $phonenumber;
        }

        return $fullNumber;
    }

    public function change_phone(Request $request){
        $data = $request->all();

        if (($data['phonenumber'] = $this->phoneValidation($data['phonenumber'], $data['phoneCountry'])) == false) {
            return redirect()->back()
                      ->with('status', 'Phone Validation Error!')
                      ->withInput();
        }

        $data['phone_code'] = rand(1000, 9999);
        $country_name = Countries::where('cca2', $data['phoneCountry'])->first()->name->common;
        
        $admin = Auth::guard('admin')->user();

        if($admin->phone_number == $data['phonenumber'])
        {
            return back()->with('error', 'You have already this Phone Number.');
        }
        
        $admin->phone_number = $data['phonenumber'];
        $admin->phone_verified = 0;
        $admin->phone_code = $data['phone_code'];
        $admin->country_code = $data['phoneCountry'];
        $admin->country_name = $country_name;
        $admin->save();

        $this->SendVerifyCode($admin['phone_number'], $admin['phone_code']);

        return back()->with('status', 'Phone Number changed successfully, you have to verify Phone');

    }

    public function SendVerifyCode($phone_number, $verify_code) {
        $url = 'http://api.infobip.com/sms/1/text/single';

        $header = [
            'defaults' => array(
            "exceptions" => true,
            "decode_content" => true),
            'verify' => false,
            // 'proxy' => '127.0.0.1:8888',
        ];

        $param = [
            'from' => 'ShareApps',
            'to' => $phone_number,
            'text' => 'Verify Code : '.$verify_code,
        ];

        $meta = [
            'accept' => 'application/json',
            'authorization' => 'Basic TXljb3JlbW9iaWxlOkhhcHB5MTIzNA==',
            'content-type' => 'application/json',
        ];

        $client = new Guzzle($header);
        $response = $client->request('POST', $url, ['json' => $param, 'headers' => $meta]);

        // Check response ...
        $response = $response->getBody()->getContents();

        // Your code here ..
    }

    public function change_email(Request $request)
    {
        if (Auth::guard('admin')->user()->id) {
            $obj_admin = Auth::guard('admin')->user();

            //if Verified email
            if($obj_admin->email == $request->input('email'))
            {
                return back()->with('error', 'You have already this email address.');
            }
            //if Email already exist
            if($this->isExistEmail($request->input('email')) == true)
            {
                return back()->with('error', 'This email address has already been taken.');
            }
            
            $emailverify_code = rand(1000, 9999);
            $obj_admin->email = $request->input('email');
            $obj_admin->verified = 0;
            $obj_admin->email_code = $emailverify_code;
            $obj_admin->save();
            if($obj_admin->email == $request->input('email')){
                Mail::to($obj_admin->email)->send(new OrderShipped());
              return back()->with('status','Confirmation email has been send. please check your email.');
            }else
                return back()->with('error', 'Something went wrong in email updating or email verification sending.');
        } else {
            return back()->with('error', 'You have no access for this action');
        }
    }

    public function isExistEmail($email){
        $user = DB::table('admins')->where(array('email' => $email))->count();
        if($user != 0){
            return true;
        }else{
            return false;
        }
    }

    public function email_active(Request $request)
    {
        $admin = Auth::guard('admin')->user();
        if($request->code == $admin->email_code)
        {
            $admin->verified = 1;
            $admin->save();
            return back()->with('status','Email Verified.');
        }
        return back()->with('error','Code Not match.');
    }

    public function phone_active(Request $request)
    {
        $admin = Auth::guard('admin')->user();
        if($request->phone_code == $admin->phone_code)
        {
            $admin->phone_verified = 1;
            $admin->save();
            return back()->with('status','Phone Verified.');
        }
        return back()->with('error','Code Not match.');
    }

    public function admin_pages()
    {
      return view('admin.admin-pages');
    }

    public function user_management()
    {
        $users = DB::table('users')->get();
        return view('admin.admin-user-management',['users' => $users]);
    }

    public function delete_user(Request $request)
    {
      $user_id = $request->user_id;
      DB::table('users')->where('id', '=', $user_id)->delete();
    }

    public function admin_management()
    {
      if(Auth::guard('admin')->user()->user_role == 2){
        $admins = DB::table('admins')->get();
        return view('admin.admin-management',['admins' => $admins]);
      }
      else{
        return redirect()->route('admin.createapp');
      }
    }

    protected function validator_add_admin(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:admins',
        ]);
    }

    protected function create_add_admin(array $data)
    {
        return Admin::create([
            'name' => $data['name'],
            'company_name' => "Shareapp",
            'email' => $data['email'],
            'phone_number' => "123456789",
            'image' => "111",
            'password' => bcrypt($data['password']),
            'country_code' => "unknown",
            'country_name' => "unknown",
            'phone_code' => "1111",
        ]);
    }

    public function add_admin(Request $request)
    {
      $data = $request->all();
      $this->validator_add_admin($data)->validate();
      $data = $this->create_add_admin($data);

      $admin = Admin::find($data['id']);
      $admin->user_role = $request['userrole'];
      $admin->verified = 1;
      $admin->phone_verified = 1;
      $admin->save();

      return back()->with('status', 'Have been created new Admin');
    }

    public function get_admin_info(Request $request){
      $admin_id = $request->admin_id;
      $obj_admin = Admin::find($admin_id);

      return $obj_admin;
    }

    public function edit_admin(Request $request)
    {
      $admin = Admin::find($request['userid']);
      $current_password = $admin->password;
      
      $before_string1 = $admin->name.$admin->email.$admin->user_role;
      $before_string2 = $admin->name.$admin->email.$admin->user_role.$admin->password;
      
      $admin->name = $request['name'];
      $admin->email = $request['email'];
      $admin->user_role = $request['userrole'];
      if($request['password'] != "" && Hash::Check($request['password'], $current_password) == false){
        $admin->password = bcrypt($request['password']);
      }
      $admin->save();
      
      $after_string1 = $admin->name.$admin->email.$admin->user_role;
      $after_string2 = $admin->name.$admin->email.$admin->user_role.$admin->password;
      
      if($before_string1 != $after_string1 || $before_string2 != $after_string2)
      {
          return back()->with('status', 'have been edited Admin');
      }
      
      return back();
    }

    public function delete_admin(Request $request)
    {
      $admin_id = $request->admin_id;
      DB::table('admins')->where('id', '=', $admin_id)->delete();
    }
}
