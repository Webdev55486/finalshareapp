<?php

namespace App\Http\Controllers;

use Auth;
use \App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use PragmaRX\Countries\Facade as Countries;
use Propaganistas\LaravelIntl\Facades\Country;
use Propaganistas\LaravelPhone\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberType;
use SimpleSoftwareIO\SMS\Facades\SMS;
use GuzzleHttp\Client as Guzzle;
use Jrean\UserVerification\Facades\UserVerification;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.home');
    }

    public function user_profile()
    {
        $countrylists = [];
        $real_phone_number="";
        $current_phone_number = Auth::user()->phone_number;
        $countries = Country::all();
        if(Auth::user()->country_code && Auth::user()->country_code != "unknown")
        {
            $current_dialNumber = Countries::where('cca2', Auth::user()->country_code)->first()->callingCode[0];
            $dial_length = strlen($current_dialNumber);
            $real_phone_number = substr($current_phone_number,$dial_length);
        }else{
            $real_phone_number = $current_phone_number;
        }

        foreach ($countries as $key => $country) {
            try {
                $dialNumber = Countries::where('cca2', $key)->first()->callingCode[0];
                $countrylists[$key] = $country.' +'.$dialNumber;
            }
            catch (\Exception $ex) {
                continue;
            }
        }
        return view('user.profile',['countries' => $countrylists,'phone_number' => $real_phone_number]);
    }

    public function change_name(Request $request)
    {
        if(Auth::User()->id){
            $obj_user = User::find(Auth::User()->id);
            
            $before_name = $obj_user->first_name.$obj_user->last_name;
            
            $obj_user->first_name = $request->input('fname');
            $obj_user->last_name = $request->input('lname');
            $obj_user->save();
            
            $after_name = $obj_user->first_name.$obj_user->last_name;
            
            if($before_name == $after_name)
            {
                return back()->with('error', 'You have already this name');
            }
            return back()->with('status', 'User Name changed successfully');
        }
        return back()->with('error', 'Went somthing wrong');
    }

    public function change_company(Request $request)
    {
        if(Auth::User()->id){
            $obj_user = User::find(Auth::User()->id);
            
            if($obj_user->company_name == $request->input('company'))
            {
                return back()->with('error', 'You have already this Company Name');
            }
            
            $obj_user->company_name = $request->input('company');
            $obj_user->save();
            return back()->with('status', 'Company Name changed successfully');
        }
        return back()->with('error', 'Went somthing wrong');
    }

    public function change_avatar(Request $request)
    {
        if (Auth::User()->id) {
            $this->validate($request, [
                'user_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            $imageRand = rand(1000, 9999);
            $user = Auth::User();
            $user->image = Auth::User()->id."_".$imageRand;
            $user->save();

            $img = $request->user_image;
            $dst = public_path('assets/images/avatar/') . Auth::User()->image;
            $thumbnail = public_path('assets/images/avatar/') . Auth::User()->image.'_thumbnail';
            if (($img_info = getimagesize($img)) === FALSE)
                return back()->with('error', 'Image not found or not an image');

            $width = $img_info[0];
            $height = $img_info[1];

            switch ($img_info[2]) {
                case IMAGETYPE_GIF  : $src = imagecreatefromgif($img);  break;
                case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($img); break;
                case IMAGETYPE_PNG  : $src = imagecreatefrompng($img);  break;
                default : return back()->with('error', 'Unknown file type');
            }

            $tmp = imagecreatetruecolor($width, $height);
            $tmp_thumbnail = imagecreatetruecolor(200,  200);
            imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
            imagecopyresized($tmp_thumbnail, $src, 0, 0, 0, 0, 200, 200, $width, $height);
            imagejpeg($tmp, $dst . ".jpg");
            imagejpeg($tmp_thumbnail, $thumbnail . ".jpg");
            // $imageName = time().'.'.$request->image->getClientOriginalExtension();
            // $request->image->move(public_path('images/avatar'), $imageName);

            return back()->with('status', 'Your avatar changed successfully');
        } else {
            return back()->with('error', 'You have no access for this action');
        }
    }

    public function change_password(Request $request)
    {
        if (Auth::Check()) {
            $requestData = $request->All();
            $this->validateChangePassword($requestData);
            $current_password = Auth::User()->password;
            if (Hash::Check($requestData['current-password'], $current_password)) {
                $user_id = Auth::User()->id;
                $obj_user = User::find($user_id);
                $obj_user->password = bcrypt($requestData['password']);
                $obj_user->save();
                return back()->with('status', 'Password changed successfully');
            } else {
                return back()->with('error', 'Please enter correct current password');
            }
        } else {
            return redirect()->to('/');
        }
    }

    private function validateChangePassword(array $data)
    {
        $messages = [
            'current-password.required' => 'Please enter current password',
            'password.required' => 'Please enter password',
            'password.regex' => 'Must contain at least one number and one uppercase and lowercase letter, and at least 6 or more characters'
        ];

        $validator = Validator::make($data, [
            'current-password' => 'required',
        ], $messages);

        return $validator->validate();
    }

    public function phoneValidation($phonenumber, $countrycode) {
        $fullNumber = '';
        $phonenumber = ltrim($phonenumber, '0');
        $dialNumber = Countries::where('cca2', $countrycode)->first()->callingCode[0];

        if (strlen($phonenumber) < 9 || strlen($phonenumber) > 11) {
            return false;
        }
        else if (strlen($phonenumber) <= 11 && strlen($phonenumber) >= 9 ) {
            if (strpos($phonenumber, $dialNumber) === 0)
            return false;

            $fullNumber = $dialNumber.$phonenumber;
        }
        else {
            if (strpos($phonenumber, $dialNumber) !== 0)
            return false;

            $fullNumber = $phonenumber;
        }

        return $fullNumber;
    }

    public function change_phone(Request $request){
        $data = $request->all();

        if (($data['phonenumber'] = $this->phoneValidation($data['phonenumber'], $data['phoneCountry'])) == false) {
            return redirect()->back()
                      ->with('status', 'Phone Validation Error!')
                      ->withInput();
        }

        $data['phone_code'] = rand(1000, 9999);
        $country_name = Countries::where('cca2', $data['phoneCountry'])->first()->name->common;

        $user = Auth::user();
        
        if($user->phone_number == $data['phonenumber'])
        {
            return back()->with('error', 'You have already this Phone Number.');
        }
        
        $user->phone_number = $data['phonenumber'];
        $user->phone_verified = 0;
        $user->phone_code = $data['phone_code'];
        $user->country_code = $data['phoneCountry'];
        $user->country_name = $country_name;
        $user->save();

        $this->SendVerifyCode($user['phone_number'], $user['phone_code']);

        return back()->with('status', 'Phone Number changed successfully, you have to verify Phone');

    }

    public function SendVerifyCode($phone_number, $verify_code) {
        $url = 'http://api.infobip.com/sms/1/text/single';

        $header = [
            'defaults' => array(
            "exceptions" => true,
            "decode_content" => true),
            'verify' => false,
            // 'proxy' => '127.0.0.1:8888',
        ];

        $param = [
            'from' => 'ShareApps',
            'to' => $phone_number,
            'text' => 'Verify Code : '.$verify_code,
        ];

        $meta = [
            'accept' => 'application/json',
            'authorization' => 'Basic TXljb3JlbW9iaWxlOkhhcHB5MTIzNA==',
            'content-type' => 'application/json',
        ];

        $client = new Guzzle($header);
        $response = $client->request('POST', $url, ['json' => $param, 'headers' => $meta]);

        // Check response ...
        $response = $response->getBody()->getContents();

        // Your code here ..
    }

    public function change_email(Request $request)
    {
        if (Auth::User()->id) {
            $obj_user = Auth::User();

            //if Verified email
            if($obj_user->email == $request->input('email'))
                return back()->with('error', 'You have already this email address.');
            //if Email already exist
            if($this->isExistEmail($request->input('email')) == true)
                return back()->with('error', 'This email address has already been taken.');

            $obj_user->email = $request->input('email');
            $obj_user->verified = 0;
            $obj_user->save();
            if($obj_user->email == $request->input('email')){
              UserVerification::generate($obj_user);

              UserVerification::send($obj_user, 'please verify your email');

              return back()->with('status','Confirmation email has been send. please check your email.');
            }else
                return back()->with('error', 'Something went wrong in email updating or email verification sending.');
        } else {
            return back()->with('error', 'You have no access for this action');
        }
    }

    public function isExistEmail($email){
        $user = DB::table('users')->where(array('email' => $email))->count();
        if($user != 0){
            return true;
        }else{
            return false;
        }
    }
}
