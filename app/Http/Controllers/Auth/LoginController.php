<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use Auth;
use \App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function validateLogin($request)
    {
        $this->validate($request, [
            'email' => 'required', 'password' => 'required'
        ]);
    }

    protected function credentials(Request $request)
    {
        return array_merge($request->only('email', 'password'));
    }

    public function login(Request $request)
    {

        $this->validateLogin($request);

        $remember = $request->input('remember');
        $getAdmininfo = $this->credentials($request);

        $availablecheck = Auth::guard()->attempt($getAdmininfo, $remember);

        if ($availablecheck) {
            $user = Auth::guard()->user();
            if($user->verified == 0){
                Auth::guard()->logout();
                return redirect()->back()->with('status','Confirmation email has been send. please check your email.');
            }
            else if($user->phone_verified == 0){
                Session::put('userid',$user->id);
                Auth::guard()->logout();
                return redirect()->route('password.phone_verify')->with('status','Confirmation phone verify code has been send. please check your phone.');
            }
            return redirect()->route('home');
        }
        return redirect()->back()->withInput($request->only('email','remember'))->with('error', 'These credentials do not match our records.');
    }

    public function index()
    {
        return view('user.welcome');
    }

    public function showLoginForm()
    {
        return view('auth.user.login');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        return redirect('/');
    }

    public function cookieSet($name, $value, $time)
    {
        setcookie($name, $value, $time, "/");
    }

    public function isRemember($request){
        if($request->input('remember')){
            //for 30 days
            $time = time() + (86400 * 30);
            $this->cookieSet("email",$request->input('email'),$time);
            $this->cookieSet("password",$request->input('password'),$time);
        }
    }
}
