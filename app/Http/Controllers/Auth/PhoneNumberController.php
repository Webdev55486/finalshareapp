<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use Session;
use App\Http\Controllers\Controller;
use PragmaRX\Countries\Facade as Countries;
use Propaganistas\LaravelIntl\Facades\Country;
use Propaganistas\LaravelPhone\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberType;
use SimpleSoftwareIO\SMS\Facades\SMS;
use GuzzleHttp\Client as Guzzle;

class PhoneNumberController extends Controller
{
    public function phone_verify_view(){
        if(Session('userid')){
          $user = User::find(Session('userid'));
          return view('auth.user.passwords.phone',['user'=>$user]);
        }else {
          return view('auth.user.passwords.phone');
        }
    }

    public function phone_verify(Request $request){
        $user = User::find($request->userid);
        if($request->phoneverifycode == $user->phone_code){
            $user->phone_verified = 1;
            $user->save();
            return redirect('login-page')->with('status','Your phone number verified.You can Login');
        }
        else{
            return redirect()->back()->with('error','Verification code not match.');
        }
    }

    public function phone_edit_view(){

        $countrylists = [];
        $countries = Country::all();

        foreach ($countries as $key => $country) {
            try {
                $dialNumber = Countries::where('cca2', $key)->first()->callingCode[0];
                $countrylists[$key] = $country.' +'.$dialNumber;
            }
            catch (\Exception $ex) {
                continue;
            }
        }
        if(Session('userid')){
            $user = user::find(Session('userid'));
            return view('auth.user.passwords.edit-phone',['countries' => $countrylists,'user' => $user]);
        }else{
            return view('auth.user.passwords.edit-phone',['countries' => $countrylists]);
        }
    }

    public function phoneValidation($phonenumber, $countrycode) {
        $fullNumber = '';
        $phonenumber = ltrim($phonenumber, '0');
        $dialNumber = Countries::where('cca2', $countrycode)->first()->callingCode[0];

        if (strlen($phonenumber) < 9 || strlen($phonenumber) > 11) {
            return false;
        }
        else if (strlen($phonenumber) <= 11 && strlen($phonenumber) >= 9 ) {
            if (strpos($phonenumber, $dialNumber) === 0)
            return false;

            $fullNumber = $dialNumber.$phonenumber;
        }
        else {
            if (strpos($phonenumber, $dialNumber) !== 0)
            return false;

            $fullNumber = $phonenumber;
        }

        return $fullNumber;
    }

    public function phone_edit(Request $request){
        $data = $request->all();

        if (($data['phonenumber'] = $this->phoneValidation($data['phonenumber'], $data['phoneCountry'])) == false) {
            return redirect()->back()
                      ->with('status', 'Phone Validation Error!')
                      ->withInput();
        }

        $data['phone_code'] = rand(1000, 9999);
        $country_name = Countries::where('cca2', $data['phoneCountry'])->first()->name->common;

        $user = user::find($request->userid);
        $user->phone_number = $data['phonenumber'];
        $user->phone_verified = 0;
        $user->phone_code = $data['phone_code'];
        $user->country_code = $data['phoneCountry'];
        $user->country_name = $country_name;
        $user->save();

        $this->SendVerifyCode($user['phone_number'], $user['phone_code']);

        return redirect('phone-verification')->with('status','Confirmation phone verify code has been send. please check your phone.');;

    }

    public function SendVerifyCode($phone_number, $verify_code) {
        $url = 'http://api.infobip.com/sms/1/text/single';

        $header = [
            'defaults' => array(
            "exceptions" => true,
            "decode_content" => true),
            'verify' => false,
            // 'proxy' => '127.0.0.1:8888',
        ];

        $param = [
            'from' => 'ShareApps',
            'to' => $phone_number,
            'text' => 'Verify Code : '.$verify_code,
        ];

        $meta = [
            'accept' => 'application/json',
            'authorization' => 'Basic TXljb3JlbW9iaWxlOkhhcHB5MTIzNA==',
            'content-type' => 'application/json',
        ];

        $client = new Guzzle($header);
        $response = $client->request('POST', $url, ['json' => $param, 'headers' => $meta]);

        // Check response ...
        $response = $response->getBody()->getContents();

        // Your code here ..

    }
}
